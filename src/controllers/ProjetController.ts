import { Request, Response } from "express-serve-static-core";
import HomeController from "./HomeController";

export default class ProjetController
{
    /**
     * Affiche le formulaire de creation de projet
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void
    {
        res.render('pages/projet-create');
    }

    /**
     * Recupere le formulaire et insere le projet en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO Projet ("nom", "description" , "Client_id") VALUES (?, ?, ?)').run(req.body.nom, req.body.description, 1 );

        HomeController.index(req, res);
    }

    /**
     * Affiche 1 article
     * @param req 
     * @param res 
     */
    static read(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const id = req.params.id;

        const projet = db.prepare('SELECT * FROM Projet WHERE id = ?').get(id);
          console.log(req.params.id);

        res.render('pages/projet', {
            Projet: projet
        });
    }



    /**
     * Affiche le formulaire pour modifier un article
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response)
    {
        const db = req.app.locals.db;
        const id = req.params.id;

        const projet = db.prepare('SELECT * FROM Projet WHERE id = ?').get(id);

        res.render('pages/projet-update', {
            Projet: projet
        });
    }

    /**
     * Recupere le formulaire de de projet modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('UPDATE Projet SET nom = ?, description = ? WHERE id = ?').run(req.body.nom, req.body.description, req.params.id);

        HomeController.index(req, res);
    }

    /**
     * Supprime un article
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM Projet WHERE id = ?').run(req.params.id);

       HomeController.index(req, res);
    }
}