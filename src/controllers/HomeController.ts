import { Request, Response } from "express-serve-static-core";
import ProjetController from "./ProjetController";
export default class HomeController
{

    static index(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const descrip = db.prepare('SELECT * FROM Projet').all();
       
        // console.log(descrip);
        

        res.render('pages/index', {
            arrayProjet: descrip
        });
    }
    
}