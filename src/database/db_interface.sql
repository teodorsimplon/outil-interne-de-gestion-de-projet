INSERT INTO "Client" ("nom" , "adresse") 
VALUES ('bob' , '3 rue de lune, 31410 Longages'), ('ron' , '66 highway to hell Chicago') , ('gigi' , '4 farmers road Kansas');

INSERT INTO "Dev" ("nom" , "prenom" , "type")
VALUES ('cutugno' , 'toto' , 'tech lead') , ('langustiono' , 'peperoni' , 'junior') , ('snap' , 'finger' , 'senior');

INSERT INTO "Projet" ("nom" , "description" , "Client_id")
VALUES ('Projet Euclide' , 'Projet Euclide (titre original :Project Euclid) est une entreprise de collaboration entre la bibliothèque de l’université Cornell (en) et Duke University Press qui cherche à améliorer la communication scientifique en mathématiques pures et appliquées et la statistique à travers des partenariats avec des éditeurs indépendants ou de sociétés scientifiques. Il a été créé pour fournir une plateforme à de petits éditeurs de revues scientifiques pour migrer de manière économique d’une impression papier à une édition électronique1.

Par la combinaison du soutien de la part des bibliothèques abonnées et des éditeurs participants, le Projet Euclide a rendu 70 % des articles de ses revues accessibles en libre accès. En 2010, le Projet Euclide procurait un libre accès à plus d’un million de pages', 1);