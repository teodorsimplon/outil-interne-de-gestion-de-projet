-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Teodor
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-20 09:42
-- Created:       2021-12-20 09:42
PRAGMA foreign_keys = OFF;

-- Schema: mydb

BEGIN;
CREATE TABLE "Client"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" VARCHAR(45) NOT NULL,
  "adresse" VARCHAR(45) NOT NULL
);
CREATE TABLE "Dev"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" VARCHAR(45) NOT NULL,
  "prenom" VARCHAR(45) NOT NULL,
  "type" VARCHAR(45) NOT NULL
);
CREATE TABLE "Projet"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nom" VARCHAR(45) NOT NULL,
  "description" VARCHAR(45) NOT NULL,
  "Client_id" INTEGER NOT NULL,
  CONSTRAINT "fk_Projet_Client"
    FOREIGN KEY("Client_id")
    REFERENCES "Client"("id")
);
CREATE INDEX "Projet.fk_Projet_Client_idx" ON "Projet" ("Client_id");
CREATE TABLE "Dev_has_Projet"(
  "Dev_id" INTEGER NOT NULL,
  "Projet_id" INTEGER NOT NULL,
  PRIMARY KEY("Dev_id","Projet_id"),
  CONSTRAINT "fk_Dev_has_Projet_Dev1"
    FOREIGN KEY("Dev_id")
    REFERENCES "Dev"("id"),
  CONSTRAINT "fk_Dev_has_Projet_Projet1"
    FOREIGN KEY("Projet_id")
    REFERENCES "Projet"("id")
);
CREATE INDEX "Dev_has_Projet.fk_Dev_has_Projet_Projet1_idx" ON "Dev_has_Projet" ("Projet_id");
CREATE INDEX "Dev_has_Projet.fk_Dev_has_Projet_Dev1_idx" ON "Dev_has_Projet" ("Dev_id");
COMMIT;
