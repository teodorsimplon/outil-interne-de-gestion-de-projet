# outil interne de gestion de projet

Contexte du projet
Pour chaque projet, nous aurons besoin des informations suivantes :

Un nom de projet
Un description longue du projet
Cet outil nous permettra d'assigné des développeurs de l'agence sur chaque projet. Certains developpeur ne travaillerons que sur un seul projet, certains travaillerons sur plusieurs projets en même temps. Il est possible que certains developpeurs ne soient assigné à aucun projet.

Nous aurons aussi besoin de savoir pour à quel client appartient chaque projet.

Pour les développeurs de l'agence, nous devons connaitre leur nom, leur prénom et leur niveau (junior, sénior ou tech lead). Pour les clients, nous devons connaitre, leur nom et adresse.

Merci d'avoir choisi ce logiciel, pour pouvoir l'utiliser correctement vous devez suivre les conseils suivants:

    • utilisez le bouton cloner de la plate-forme gitlab pour copier le lien du projet
    • ouvrez un terminal sur votre pc dans le dossier ou vous voulez stocker ce logiciel
    • avec la ligne de commande git clone + lien copié vous téléchargerez localement le projet
    • faites un clic droit sur le dossier du projet et ouvrez-le avec VSCode
    • une fois le projet ouvert, vous pouvez utiliser le terminal VSCode pour installer les packages npm avec la ligne de commande npm install ou npm I
    • vous devrez également installer le typescript avec la ligne de commande npm install -g typescript pour une installation globale ou pour le projet uniquement avec la ligne de commande npm install typescript si vous rencontrez des erreurs
    • lancer le script npm avec la ligne de démarrage npm dans le terminal
    • une fois que vous recevez la page Web dans le terminal, vous pouvez cliquer à l'aide du bouton ctrl enfoncé et vous serez redirigé vers le site du projet


Si vous rencontrez des erreurs concernant la base de données, procédez comme suit

            ▪ vous allez dans data.db où vous sélectionnez tout le texte et vous supprimez et enregistrez le fichier vide
            ▪ vous allez dans server.ts et décommentez le code de la ligne 25 à la ligne 30 et enregistrez une fois le fichier, s'il ne lance pas npm utilisez npm start commande
            ▪ commentez les lignes de code de server.ts en sélectionnant le texte et en appuyant sur ctrl+shift+forward slash
            ▪ vérifiez dans le fichier data.db pour vous assurer que les données ont été importées et profitez du logiciel

afin de modifier le code de ce projet, vous devez utiliser les lignes de commande suivantes : 
git add .
git commit -m "changements effectués"
git pull(seulement s'il y a eu d'autres push depuis le dernier sur la même branche)
git push

